<?php 
// $Id$

/**
 * @file
 * Theming functions
 * 
 * All functionality relating to the theming of hierarchical solr results,
 * including relavant hooks and preprocesses.
 */

/**
 * implementation of hook_theme
 *
 * @return array
 */
function apachesolr_hierarchy_theme() {
  return array(
    'hierarchical_search_results' => array(
      'template' => 'hierarchical_search_results',
      'arguments' => array('tree' => NULL, 'config' => NULL, 'depth' => 0),
    ),
    'hierarchical_search_result' => array(
      'template' => 'hierarchical_search_result',
      'arguments' => array('tree_node' => NULL, 'config' => NULL, 'depth' => 0),
    ),
  );
}

/**
 * implementation of hook_theme_registry_alter
 * 
 * @param $theme_registry
 */
function apachesolr_hierarchy_theme_registry_alter(&$theme_registry) {
  // add theme directory to theme path suggestions 
  // so that custom template overrides are found
  array_unshift($theme_registry['hierarchical_search_results']['theme paths'], path_to_theme());
  array_unshift($theme_registry['hierarchical_search_result']['theme paths'], path_to_theme());
}

/**
 * menu callback to display hierarchical search results.
 * 
 * re-implementation of apachesolr_search_view() and search_data().
 * 
 * @param array $config
 */
function apachesolr_hierarchy_result_page($config) {
  // initialise
  $type = $config['#type'];
  $search_type = $config['#search-type'];
  $results = '';

  $keys = trim(search_get_keys());  
  $filters = isset($_GET['filters']) ? trim($_GET['filters']) : '';
  $solrsort = isset($_GET['solrsort']) ? $_GET['solrsort'] : '';
  $page = isset($_GET['page']) ? $_GET['page'] : 0;
  
  // add default type filter
  if (strpos($filters, 'type:' . $type) === FALSE) {
    $filters .= ' type:' . $type;
  }
  try {
    // execute search
    $results = apachesolr_search_execute($keys, $filters, $solrsort, 'search/' . $search_type, $page);
    
    // here is where the magic happens
    $tree = _transform_solr_to_tree($results);
    
    // theme tree
    $output = (empty($tree)) ? '' : theme('hierarchical_search_results', $tree['#children'], $config);
  }
  catch (Exception $e) {
    // log errors
    watchdog('Apache Solr Hierarchy', nl2br(check_plain($e->getMessage())), NULL, WATCHDOG_ERROR);
    apachesolr_failure(t('Solr search'), $keys);
  }

  if ($output) {
    // add css + js
    drupal_add_css(drupal_get_path('module', 'apachesolr_hierarchy') .'/css/apachesolr_hierarchy.css');
    drupal_add_js(drupal_get_path('module', 'apachesolr_hierarchy') .'/js/apachesolr_hierarchy.js');
    
    // pass settings through to jQuery
    $jquery_settings = array(
      'apachesolr_hierarchy' => array(
        'module_image_path' => drupal_get_path('module', 'apachesolr_hierarchy') .'/images',
        'expand_default' => variable_get('apachesolr_hierarchy_expand_default', 0),
      ),
    );
    drupal_add_js($jquery_settings, 'setting');
    
    // add generic theming 
    $output = theme('box', t('Search results'), '<div id="hierarchical-search-results">' . $output . '</div>');
  
  } else {
    // no results
    $output = theme('box', $config['#null-results'], '');
  }
  // add search form to output
  $form = drupal_get_form('search_form', NULL, $keys, $search_type);
  return $form . $output;
}


/**
 * preprocess for search results
 * 
 * @param array $variables
 */
function template_preprocess_hierarchical_search_results(&$variables) {
  // initialise
  $config = $variables['config'];
  $tree = $variables['tree'];
  $depth = $variables['depth'];
  
  // generate css classes
  $variables['classes'][] = 'hierarchy-parent';  
  $variables['classes'][] = 'apachesolr_search-results';
  $variables['classes'][] = 'depth-' . $depth;
  
  // build composite class string
  $variables['classes_inline'] = implode(' ', $variables['classes']);

  if (is_array($tree)) {
    // results exist
    
    foreach ($tree as $sibling) {
      if (!empty($sibling)) {
        $variables['results'] .= theme('hierarchical_search_result', $sibling, $config, $depth);
      }
    }
  }
}

/**
 * preprocess for a single search result
 * 
 * @param array $variables
 */
function template_preprocess_hierarchical_search_result(&$variables) {
  // initialise
  $config = $variables['config'];
  $tree_node = $variables['tree_node'];
  $depth = $variables['depth'];
  $document = $tree_node['#result']['node'];
  $title = $tree_node['#title'];
    
  $variables['title'] = l($title, $document->path);
  
  // prepare result-specific template variables
  if ($document) {
    $variables['type'] = $document->type;
    // filter out snippet for empty body text
    if (trim($document->body) != '...') $variables['body'] = $document->body;
    
    // this isn't of much use currently
    // $variables['info'] = theme('item_list', $variables['info_split'], NULL, 'ul', array('class' => 'apachesolr_hierarchy-results-info'));
  }
  
  // not sure if i want to use standard search result template
  // but here's where to do it if i do
  
//  $searches = array('<li>', '</li>', '<dt>', '<dd>');
//  $variables['result'] = str_replace($searches, '', theme('search_result', $tree_node['#result'], $document->type));

  // generate css classes
  $variables['classes'][] = 'hierarchical-search-result';  
  $variables['classes'][] = 'depth-' . $depth;
  $variables['classes'][] = ($depth == 0) ? 'node-type-' . $config['#type'] : ''; 
  $variables['classes'][] = (empty($tree_node['#children'])) ? 'no-children' : 'has-children';
  $variables['classes'][] =  ($document) ? 'match' : 'no-match';
  
  // build composite class string
  $variables['classes_inline'] = implode(' ', $variables['classes']);
  
  // add children to template
  $variables['children'] = theme('hierarchical_search_results', $tree_node['#children'], $config, $depth + 1);
}


