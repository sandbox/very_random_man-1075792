INSTALLATION

- Module pre-requisite: apachesolr, jquery_update, relativity
- Download and enable this module.

SET UP

- Apache SOLR must be running 
  (/admin/settings/apachesolr)
  
- You will need to have enabled relativity for at least one content type and set allowable child node types.
  (/admin/settings/relativity)

- Create parent-child relationships between at least one node  
  
- Re-index solr.



