jQuery(document).ready(function(){
	// determine expand/collapse default
	var expand_default = Drupal.settings.apachesolr_hierarchy.expand_default;
	
	// define icon
    var icon_path = Drupal.settings.apachesolr_hierarchy.module_image_path + '/pixel.gif';
    var icon_markup = '<img src="' + icon_path + '" class="toggle" />';
    
	// add icon to all relevant tree nodes
	$('li.has-children').prepend(icon_markup);
	
	// apply default class to tree nodes
	$('li.hierarchical-search-result > img.toggle').each(function(index) {
		
		if (($(this).parent().hasClass('no-match')) || expand_default == 1) {
			// tree node has no result attached to it so expand on page load
			$(this).addClass('toggle-expanded');
			
		} else {	
			// set collapsed by default
			$(this).addClass('toggle-collapsed');
			$(this).siblings('ul.hierarchy-parent').hide();
		}
	});	

	// set click handler to expand collapsed tree nodes
	$('li.hierarchical-search-result > img.toggle-collapsed').live('click', function () {
		$(this).removeClass('toggle-collapsed');
		$(this).siblings('ul.hierarchy-parent').toggle('slow');
		$(this).addClass('toggle-expanded');
	});
	
	// set click handler to collapsed expanded tree nodes
	$('li.hierarchical-search-result > img.toggle-expanded').live('click', function () {
		$(this).removeClass('toggle-expanded');
		$(this).siblings('ul.hierarchy-parent').toggle('slow');
		$(this).addClass('toggle-collapsed');
	});  
	
});
