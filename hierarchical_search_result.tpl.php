<li class="<?php print $classes_inline; ?>">
  <h3><?php print $title; ?></h3>
  
  <div class="result">
    <div class="body">
      <?php print $body; ?>
    </div>
    
    <?php print $info; ?>
    <?php //print $result; ?>
  </div>
  <?php print $children; ?>
</li>